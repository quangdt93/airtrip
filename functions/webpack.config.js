module.exports={
    mode: 'none',
    entry: './app/app.jsx',
    output: {
        path: __dirname,
        filename: '../public/user.js'
    },
    node: {
      net: 'empty', fs: 'empty', __dirname: true
    },
    module: {
        rules:[
            {
                test: /\.jsx$/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/preset-env', '@babel/preset-react']
                },
                exclude: [/node_modules/]
            }
        ]
    }
}
