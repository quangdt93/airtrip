const functions = require('firebase-functions');
const firebase = require('firebase/app')
require('firebase/auth');
require('firebase/database');
const express = require('express');
const app = express();
app.set('view engine', 'ejs');
app.set('views', './views');

app.get("/", (req, res) => {
    res.render(`home`);
})
const config = {
    apiKey: "AIzaSyDhMTOMsfJERfDkgyAi3eya_Jc9zh3IArc",
    authDomain: "quangdang-196507.firebaseio.com",
    databaseURL: "https://quangdang-196507.firebaseio.com",
    storageBucket: "bucket.appspot.com"
};
firebase.initializeApp(config);
exports.app = functions.https.onRequest(app);
