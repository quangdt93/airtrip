import React from 'react';
import firebase from 'firebase/app';
require('firebase/auth');
require('firebase/database');

const config = {
    apiKey: "AIzaSyDhMTOMsfJERfDkgyAi3eya_Jc9zh3IArc",
    authDomain: "quangdang-196507.firebaseio.com",
    databaseURL: "https://quangdang-196507.firebaseio.com",
    storageBucket: "bucket.appspot.com"
};
firebase.initializeApp(config);

class UserList extends React.Component {
    UNSAFE_componentWillMount() {
        firebase.database().ref('/Users').once('value').then((users) => {
            this.setState({user: users.val()})
        })
    }

    constructor(props) {
        super(props);
        this.state = {
            user: []
        }
    }

    renderUserList() {
        return this.state.user.map((user) => {
            const {id, name, mail, address} = user;
            return(
                <tr key={id}>
                    <th scope={'row'}>{id + 1}</th>
                    <td>{name}</td>
                    <td>{mail}</td>
                    <td>{address}</td>
                </tr>
            );
        })
    }
    render() {
        return (
            <table className={'table'}>
                <thead>
                <tr>
                    <th scope={"col"}>ID</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Address</th>
                </tr>
                </thead>
                <tbody>
                    {this.renderUserList()}
                </tbody>
            </table>
        )
    }
}
export default UserList;

