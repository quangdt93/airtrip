import React from 'react'
import ReactDom from 'react-dom';
import UserList from "./components/ListUser.jsx";

ReactDom.render(
    <div>
        <UserList />
    </div>,
    document.getElementById("Root")
);
